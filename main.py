import pandas 
from apyori import apriori
import sklearn 
import sklearn.model_selection

def load_data(filename):
    data = pandas.read_csv(filename)
    return data
data = load_data("store_data.csv")

def preprocess(data):
    item_list = data.unstack().dropna().unique()
    print(item_list)
    print("# items:",item_list)

item_list = data.unstack().dropna().unique()
print(item_list)
print("# items:",len(item_list))

train, test = sklearn.model_selection.train_test_split(data, test_size=.10)
print("train",train)
print("test",test)

train = train.T.apply(lambda x: x.dropna().tolist()).tolist()
print("transform")
for i in train[:10]:
    print(i)

result = list(apriori(train, min_support=0.0045,min_confidende=0.2,min_lift=3,min_length=2))
print("result")
print(result)

for rule in result:
    print(rule[0])
    print("-------------------------------------------------")


#item_list = data.join
